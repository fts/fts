/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package daemon

import (
	"fmt"
	"os"
	"os/user"
	"testing"
)

func TestGetName(t *testing.T) {
	bin := GetBinaryName()
	t.Log(bin)
}

func TestCountProc(t *testing.T) {
	bin := GetBinaryName()
	count := CountProcessesWithName(bin)
	t.Log(count)
	if count < 1 {
		t.Error("Expecting at least one")
	}
}

func TestDropPrivilegesSameUID(t *testing.T) {
	u, err := user.LookupId(fmt.Sprint(os.Geteuid()))
	if err != nil {
		t.Fatal(err)
	}
	g, err := user.LookupGroupId(fmt.Sprint(os.Getegid()))
	if err != nil {
		t.Fatal(err)
	}
	err = DropPrivileges(u.Username, g.Name)
	if err != nil {
		t.Error("Setting the same user and group expected to work: ", err)
	}
}

func TestDropPrivilegesRoot(t *testing.T) {
	err := DropPrivileges("root", "root")
	t.Log(err)
	if os.Getuid() == 0 {
		if err != nil {
			t.Error("Root is expected to be able to change: ", err)
		}
	} else {
		if err == nil {
			t.Error("Non-root is expected to fail")
		}
	}
}
