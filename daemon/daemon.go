/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package daemon

// #include <unistd.h>
import "C"

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strconv"
	"syscall"
)

// GetBinaryName returns the name of the current binary
func GetBinaryName() string {
	binFull := os.Args[0]
	return path.Base(binFull)
}

// CountProcessesWithName returns how many processes are called like 'name' (only suffix check)
func CountProcessesWithName(name string) int {
	count := 0

	procEntries, err := ioutil.ReadDir("/proc")
	if err != nil {
		logrus.Fatal(err)
	}

	for _, entry := range procEntries {
		if _, err := strconv.Atoi(entry.Name()); err != nil {
			continue
		}

		exe := path.Join("/proc", entry.Name(), "exe")
		target, err := os.Readlink(exe)
		if err != nil {
			logrus.WithError(err).Debug("Failed to follow exe link")
			continue
		}

		if len(target) > 0 && path.Base(target) == name {
			count++
		}
	}

	return count
}

// DropPrivileges switches user and group for the running process
// Only works for Unix
func DropPrivileges(userName, groupName string) error {
	userInfo, err := user.Lookup(userName)
	if err != nil {
		return err
	}
	groupInfo, err := user.LookupGroup(groupName)
	if err != nil {
		return err
	}

	uid, err := strconv.Atoi(userInfo.Uid)
	if err != nil {
		return err
	}
	gid, err := strconv.Atoi(groupInfo.Gid)
	if err != nil {
		return err
	}

	if os.Getuid() == uid && os.Getgid() == gid {
		logrus.Infof("No need to drop privileges, running as %d %d", syscall.Getuid(), syscall.Getgid())
		return nil
	}

	// Now, here is the thing
	// syscall.Setuid and Setgid will fail saying that it is *not* supported for Linux
	// (see https://github.com/golang/go/issues/1435)
	// Alternative calls are affected all the same: only the uid and gid of the *current* thread changes
	// From that ticket, it is said that the glibc implementation signals other threads to switch too,
	// so we call directly the C version here
	if r, errno := C.setgid(C.__gid_t(gid)); r != 0 {
		return fmt.Errorf("Failed to switch group: %s", errno)
	}
	if r, errno := C.setuid(C.__uid_t(uid)); r != 0 {
		return fmt.Errorf("Failed to switch user: %s", errno)
	}

	logrus.Infof("Dropped privileges (%d %d)", syscall.Getuid(), syscall.Getgid())
	return nil
}
