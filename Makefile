GO := $(shell which go)

all: build

test:
	$(GO) test `go list ./... | grep -v vendor`

build:
	$(GO) install -v `go list ./... | grep -v vendor`
