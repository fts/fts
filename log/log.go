/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package log

import (
	"bytes"
	"fmt"
	"github.com/Sirupsen/logrus"
	"os"
	"path"
	"sort"
	"strings"
)

// FTSLogFormatter formats the log output in a compatible manner with fts3server.log
type FTSLogFormatter struct{}

const (
	nocolor = 0
	red     = 31
	green   = 32
	yellow  = 33
	blue    = 34
	gray    = 37
)

func formatLevel(entry *logrus.Entry) string {
	levelText := strings.ToUpper(entry.Level.String())
	isTerminal := logrus.IsTerminal(entry.Logger.Out)
	if isTerminal {
		var levelColor int
		switch entry.Level {
		case logrus.DebugLevel:
			levelColor = gray
		case logrus.WarnLevel:
			levelColor = yellow
		case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
			levelColor = red
		default:
			levelColor = blue
		}
		return fmt.Sprintf("\x1b[%dm%-7s\x1b[0m", levelColor, levelText)
	}
	return fmt.Sprintf("%-7s", levelText)
}

// Format the entry according to the traditional FTS3 server log formatting
func (*FTSLogFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	fmt.Fprintf(b, "%s %s; %s [", formatLevel(entry), entry.Time.Format("Mon Jan 02 15:04:05 2006"), entry.Message)

	keys := make([]string, 0, len(entry.Data))
	for k := range entry.Data {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for i, k := range keys {
		fmt.Fprintf(b, "%s=\"%v\"", k, entry.Data[k])
		if i < len(keys)-1 {
			b.WriteByte(' ')
		}
	}
	b.WriteString("]\n")

	return b.Bytes(), nil
}

// InitLogging initializes the logging format and level
func InitLogging(logLevel string) {
	switch strings.ToUpper(logLevel) {
	case "DEBUG":
		logrus.SetLevel(logrus.DebugLevel)
	case "WARNING":
		logrus.SetLevel(logrus.WarnLevel)
	case "ERROR":
		logrus.SetLevel(logrus.ErrorLevel)
	case "INFO":
		logrus.SetLevel(logrus.InfoLevel)
	default:
		logrus.Warn("Unknown log level ", logLevel, ", using INFO")
		logrus.SetLevel(logrus.InfoLevel)
	}
	logrus.SetFormatter(&FTSLogFormatter{})
}

// RedirectLog redirects the log into basePath/logName
// It aborts the execution if it fails to open the log
func RedirectLog(basePath, logName string) {
	logPath := path.Join(basePath, logName)
	logOut, err := os.OpenFile(logPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		logrus.WithError(err).Fatal("Could not open the log file")
	}
	logrus.SetOutput(logOut)
}
