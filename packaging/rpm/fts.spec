%global _dwz_low_mem_die_limit 0


Name:		fts
Version:	4.0.0
Release:	1%{?dist}
Summary:	File Transfer Service V4
Group:		System Environment/Daemons
License:	ASL 2.0
URL:		http://fts3-service.web.cern.ch/
# The source for this package was pulled from upstream's vcs.  Use the
# following commands to generate the tarball:
#  git clone https://gitlab.cern.ch/fts/fts.git -b master --depth=1 fts-4.0.0
#  cd fts-4.0.0
#  cd ..
#  tar --exclude-vcs -vczf fts-4.0.0.tar.gz fts-4.0.0
Source0:		%{name}-%{version}.tar.gz
ExclusiveArch:	%{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 %{arm}}


BuildRequires:	%{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires:	/bin/which
BuildRequires:	systemd
BuildRequires:  gfal2-devel >= 2.9.0

# Metapackage
Requires:	fts-bringonline%{?_isa} = %{version}-%{release}
Requires:	fts-msg%{?_isa} = %{version}-%{release}

%description
The File Transfer Service V4 is a drop-in replacement of some V3 components.
It is a service and a set of command line tools for managing third party
transfers, most importantly the aim of FTS3 is to transfer the data produced
by CERN's LHC into the computing GRID.

# Bring online subsystem
%package bringonline
Summary:	Bring online subsystem
Group:		System Environment/Daemons

Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

%description bringonline
Bring Online subsystem. It is the responsible for staging operations.

# Messaging bridge subsystem
%package msg
Summary:	FTS Messaging bridge subsystem
Group:		System Environment/Daemons

Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

%description msg
FTS Messaging subsystem. Bridges internal messaging with an external
STOMP broker.


%prep
%setup -q -n %{name}-%{version}

%build
mkdir -p ./_build/src/gitlab.cern.ch/fts/
ln -s $(pwd) ./_build/src/gitlab.cern.ch/fts/fts
export GOPATH=$(pwd)/_build:%{gopath}

cd ./_build/src/gitlab.cern.ch/fts/fts
make test
make

%install
install -d %{buildroot}%{_sbindir}
install -d %{buildroot}%{_unitdir}
install -p -m 0755 ./_build/bin/* %{buildroot}%{_sbindir}
install -p -m 0644 ./scripts/systemd/* %{buildroot}%{_unitdir}


# Bringonline scriptlets
%post bringonline
/bin/systemctl daemon-reload > /dev/null 2>&1 || :

%preun bringonline
if [ $1 -eq 0 ] ; then
	/bin/systemctl stop fts-bringonline.service > /dev/null 2>&1 || :
fi

%postun bringonline
if [ "$1" -ge "1" ] ; then
	/bin/systemctl try-restart fts-bringonline.service > /dev/null 2>&1 || :
fi


# Messaging scriptlets
%post msg
/bin/systemctl daemon-reload > /dev/null 2>&1 || :

%preun msg
if [ $1 -eq 0 ] ; then
	/bin/systemctl stop fts-msg-bridge.service > /dev/null 2>&1 || :
fi

%postun msg
if [ "$1" -ge "1" ] ; then
	/bin/systemctl try-restart fts-msg-bridge.service > /dev/null 2>&1 || :
fi


# Files
%files
%defattr(-,root,root,-)
%doc LICENSE README.md

%files bringonline
%defattr(-,root,root,-)
%{_sbindir}/fts-bringonline
%{_unitdir}/fts-bringonline.service

%files msg
%defattr(-,root,root,-)
%{_sbindir}/fts-msg-bridge
%{_unitdir}/fts-msg-bridge.service


# Changelog
%changelog
* Tue Mar 21 2017 Alejandro Alvarez Ayllon <aalvarez@cern.ch> - 4.0.0-1
- First package


