/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"gitlab.cern.ch/flutter/stomp"
	"strconv"
	"testing"
	"time"
)

type MockMessage struct {
	params            stomp.SendParams
	destination, body string
}

type MockProducer struct {
	messages chan MockMessage
}

func NewMockProducer() *MockProducer {
	return &MockProducer{
		messages: make(chan MockMessage),
	}
}

func (m *MockProducer) Send(destination, msg string, params stomp.SendParams) error {
	m.messages <- MockMessage{
		params:      params,
		destination: destination,
		body:        msg,
	}
	return nil
}

func TestSimpleOutboundQueue(t *testing.T) {
	jsonMessage := `{"field": "value", "vo": "dteam"}`

	producer := NewMockProducer()
	defer close(producer.messages)
	outboundChannel := NewOutbound(producer, false, "my.destination")
	defer close(outboundChannel)

	outboundChannel <- []byte(jsonMessage)

	msg := <-producer.messages

	if msg.destination != "/queue/my.destination" {
		t.Error("Expected a queue as a destination")
	}
	// Stupid trailing byte required for backwards compatibility
	if msg.body[0:len(msg.body)-1] != jsonMessage {
		t.Errorf("Unexpected body: '%s'", msg.body)
	}

	if msg.params.Headers["vo"] != "dteam" {
		t.Error("Unexpected vo header:", msg.params.Headers["vo"])
	}

	ttl, _ := strconv.Atoi(msg.params.Headers["ttl"])
	if time.Duration(ttl)*time.Millisecond != time.Duration(*ttlFlag)*time.Hour {
		t.Error("Unexpected ttl:", msg.params.Headers["ttl"])
	}

	if msg.params.ContentType != "application/json" {
		t.Error("Expecting application/json, got", msg.params.ContentType)
	}

	if !msg.params.Persistent {
		t.Error("Expecting a persistent message")
	}
}

func TestSimpleOutboundTopic(t *testing.T) {
	jsonMessage := `{"field": "value", "vo": "dteam"}`

	producer := NewMockProducer()
	defer close(producer.messages)
	outboundChannel := NewOutbound(producer, true, "my.destination")
	defer close(outboundChannel)

	outboundChannel <- []byte(jsonMessage)

	msg := <-producer.messages

	if msg.destination != "/topic/my.destination" {
		t.Error("Expected a queue as a destination")
	}
}
