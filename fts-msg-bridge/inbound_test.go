/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"gitlab.cern.ch/flutter/go-dirq"
	"io/ioutil"
	"testing"
)

func GetTempDir(t *testing.T) (string, *dirq.Dirq) {
	dirqPath, err := ioutil.TempDir("/tmp", "dirq")
	if err != nil {
		t.Fatal(err)
	}
	handle, err := dirq.New(dirqPath)
	if err != nil {
		t.Fatal(err)
	}
	return dirqPath, handle
}

func TestProduceStatus(t *testing.T) {
	dirqPath, dirqHandle := GetTempDir(t)
	defer dirqHandle.Close()

	if err := dirqHandle.Produce([]byte(`SS {"field": "value"}`)); err != nil {
		t.Fatal(err)
	}

	channel, terminate, err := Inbound(dirqPath)
	if err != nil {
		t.Fatal(err)
	}

	msg := <-channel

	if msg.Type != StatusChange {
		t.Error("Unexpected message type:", msg.Type)
	}

	if string(msg.Body) != `{"field": "value"}` {
		t.Errorf("Unexpected message body: '%s'", msg.Body)
	}

	close(terminate)
}

func TestProduceStart(t *testing.T) {
	dirqPath, dirqHandle := GetTempDir(t)
	defer dirqHandle.Close()

	if err := dirqHandle.Produce([]byte(`ST {"field": "value"}`)); err != nil {
		t.Fatal(err)
	}

	channel, terminate, err := Inbound(dirqPath)
	if err != nil {
		t.Fatal(err)
	}

	msg := <-channel

	if msg.Type != TransferStart {
		t.Error("Unexpected message type:", msg.Type)
	}

	if string(msg.Body) != `{"field": "value"}` {
		t.Errorf("Unexpected message body: '%s'", msg.Body)
	}

	close(terminate)
}

func TestProduceCompletion(t *testing.T) {
	dirqPath, dirqHandle := GetTempDir(t)
	defer dirqHandle.Close()

	if err := dirqHandle.Produce([]byte(`CO {"field": "value"}`)); err != nil {
		t.Fatal(err)
	}

	channel, terminate, err := Inbound(dirqPath)
	if err != nil {
		t.Fatal(err)
	}

	msg := <-channel

	if msg.Type != TransferCompletion {
		t.Error("Unexpected message type:", msg.Type)
	}

	if string(msg.Body) != `{"field": "value"}` {
		t.Errorf("Unexpected message body: '%s'", msg.Body)
	}

	close(terminate)
}
