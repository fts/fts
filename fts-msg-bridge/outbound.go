/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/stomp"
	"time"
)

var ttlFlag = flag.Int("TTL", 24, "Time to live, in hours")

// Producer is where the messages are forwarded to
type Producer interface {
	Send(destination, msg string, params stomp.SendParams) error
}

// NewOutbound returns a write channel and spawns a new goroutine that redirects messages written
// to this channel into producer
func NewOutbound(producer Producer, useTopics bool, destination string) chan<- []byte {
	ttlDuration := time.Duration(*ttlFlag) * time.Hour

	if useTopics {
		destination = "/topic/" + destination
	} else {
		destination = "/queue/" + destination
	}

	channel := make(chan []byte, 100)
	go func() {
		for msg := range channel {
			attrs := make(map[string]interface{})

			if err := json.Unmarshal(msg, &attrs); err != nil {
				logrus.WithError(err).Warn("Expecting well formed json")
				continue
			}

			var voRaw interface{}
			var ok bool
			if voRaw, ok = attrs["vo"]; !ok {
				voRaw, ok = attrs["vo_name"]
			}
			vo, ok := voRaw.(string)

			// Additional space due to legacy applications dropping a trailing \EOT
			err := producer.Send(destination, string(msg)+" ", stomp.SendParams{
				Persistent:  true,
				ContentType: "application/json",
				Headers: map[string]string{
					"vo":  vo,
					"ttl": fmt.Sprint(int(ttlDuration.Seconds()) * 1000),
				},
			})
			if err != nil {
				logrus.WithError(err).Warn("Failed to send the message to the broker")
			}
		}
	}()
	return channel
}
