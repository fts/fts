/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/stomp"
	"gitlab.cern.ch/fts/fts/daemon"
	"gitlab.cern.ch/fts/fts/log"
	"path"
	"time"
)

var runAsUser = flag.String("User", "fts3", "Run as this user")
var runAsGroup = flag.String("Group", "fts3", "Run as this group")

var messagingDirectory = flag.String("MessagingDirectory", "/var/lib/fts3", "Directory where the internal FTS3 messages are written")
var fqdn = flag.String("FQDN", "", "Full qualified domain name of this server")
var completeDestination = flag.String("COMPLETE", "transfer.fts_monitoring_complete", "Destination for completion messages")
var stateDestination = flag.String("STATE", "transfer.fts_monitoring_state", "Destination for state messages")
var startDestination = flag.String("START", "transfer.fts_monitoring_start", "Destination for start messages")
var useTopics = flag.Bool("TOPIC", true, "Destinations are topics")

var sslUse = flag.Bool("SSL", false, "Enable SSL")
var sslVerify = flag.Bool("SSL_VERIFY", true, "Verify remote peer")
var sslRootCA = flag.String("SSL_ROOT_CA", "/etc/grid-security/certificates/CERN-GridCA.pem", "CA for the remote peer")
var sslClientKeyStore = flag.String("SSL_CLIENT_KEYSTORE", "", "File with the client certificate and key")
var sslClientKeyStorePasswd = flag.String("SSL_CLIENT_KEYSTORE_PASSWORD", "", "Password for SSL_CLIENT_KEYSTORE")

var isActive = flag.Bool("ACTIVE", true, "Enable messaging")
var brokerAddress = flag.String("BROKER", "dashb-mb.cern.ch:61113", "Broker address")
var logDir = flag.String("LOGFILEDIR", "/var/log/fts3/", "Where to write the logs")
var logFileName = flag.String("LOGFILENAME", "msg.log", "Log file name")
var useBrokerCredentials = flag.Bool("USE_BROKER_CREDENTIALS", true, "Use credentials to connect to the broker")
var brokerPassword = flag.String("PASSWORD", "", "Password for the broker")
var brokerUserName = flag.String("USERNAME", "", "User for the broker")

var logLevel = flag.String("LogLevel", "INFO", "Debug level")
var useStdout = flag.Bool("stdout", false, "Output logging into stdout")

// Called by stomp when the connection has been lost
func connectionLostCallback(c *stomp.Broker) {
	reconnectWait := 10 * time.Second
	for {
		logrus.Info("Connection lost to the broker, reconnect in ", reconnectWait)
		time.Sleep(reconnectWait)
		if err := c.Reconnect(); err != nil {
			logrus.WithError(err).Warn("Failed to reconnect")
		} else {
			break
		}
	}
}

func main() {
	LoadConfiguration()
	if err := daemon.DropPrivileges(*runAsUser, *runAsGroup); err != nil {
		logrus.Fatal(err)
	}

	if !*isActive {
		logrus.Info("Messaging disabled")
		return
	}

	log.InitLogging(*logLevel)
	if !*useStdout {
		log.RedirectLog(*logDir, *logFileName)
	}

	monitoringDirPath := path.Join(*messagingDirectory, "monitoring")

	inbound, terminateInbound, err := Inbound(monitoringDirPath)
	if err != nil {
		logrus.WithError(err).Fatal("Failed to create the incoming end")
	}
	defer close(terminateInbound)

	stompParams := stomp.ConnectionParameters{
		Address:        *brokerAddress,
		EnableTLS:      *sslUse,
		Insecure:       *sslVerify,
		CaCert:         *sslRootCA,
		UserCert:       *sslClientKeyStore,
		UserKey:        *sslClientKeyStore,
		ConnectionLost: connectionLostCallback,
		ClientID:       *fqdn,
	}
	if *useBrokerCredentials {
		stompParams.Login = *brokerUserName
		stompParams.Passcode = *brokerPassword
	}

	stompProducer, err := stomp.NewProducer(stompParams)
	if err != nil {
		logrus.WithError(err).Fatal("Could not connect to the broker")
	}

	stateChannel := NewOutbound(stompProducer, *useTopics, *stateDestination)
	defer close(stateChannel)
	startChannel := NewOutbound(stompProducer, *useTopics, *startDestination)
	defer close(startChannel)
	completionChannel := NewOutbound(stompProducer, *useTopics, *completeDestination)
	defer close(completionChannel)

	for msg := range inbound {
		switch msg.Type {
		case StatusChange:
			logrus.WithField("type", "state").Debug(string(msg.Body))
			stateChannel <- msg.Body
		case TransferStart:
			logrus.WithField("type", "start").Debug(string(msg.Body))
			startChannel <- msg.Body
		case TransferCompletion:
			logrus.WithField("type", "completion").Debug(string(msg.Body))
			completionChannel <- msg.Body
		default:
			logrus.Fatal("Unexpected message type: ", msg.Type)
		}
	}
}
