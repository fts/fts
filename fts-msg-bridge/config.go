/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"github.com/Sirupsen/logrus"
	"gopkg.in/ini.v1"
)

var fts3config = flag.String("configfile", "/etc/fts3/fts3config", "FTS3 server config file")
var msgconfig = flag.String("MonitoringConfigFile", "/etc/fts3/fts-msg-monitoring.conf", "FTS3 messaging config file")

// LoadConfiguration initializes the configuration using the flags and the configuration file
func LoadConfiguration() {
	flag.Parse()

	cfg, err := ini.Load(*fts3config)
	if err != nil {
		logrus.WithError(err).Fatal("Could not open the configuration file")
	}

	rootSection, err := cfg.GetSection("")
	if err != nil {
		logrus.WithError(err).Fatal("Could not get the root section")
	}

	for _, key := range rootSection.Keys() {
		flag.Set(key.Name(), key.String())
	}

	// Call again to override values loaded from the file
	flag.Parse()

	// Load the messaging config file
	msgCfg, err := ini.Load(*msgconfig)
	if err != nil {
		logrus.WithError(err).Fatal("Could not open the messaging configuration file")
	}
	rootSection, err = msgCfg.GetSection("")
	if err != nil {
		logrus.WithError(err).Fatal("Could not get the root section for the configuration file")
	}

	for _, key := range rootSection.Keys() {
		flag.Set(key.Name(), key.String())
	}
}
