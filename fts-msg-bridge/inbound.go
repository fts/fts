/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/go-dirq"
	"time"
)

// Message types
const (
	StatusChange = iota
	TransferStart
	TransferCompletion
)

// Message is an already tagged message coming from the local dirq
type Message struct {
	Type int
	Body []byte
}

func processIncoming(dirqHandle *dirq.Dirq, out chan<- Message) {
	in := dirqHandle.Consume()
	for raw := range in {
		if raw.Error != nil {
			logrus.WithError(raw.Error).Error("Error while consuming messages")
			continue
		}
		if len(raw.Message) < 3 {
			logrus.Error("Message too short")
			continue
		}

		msg := Message{}
		var prefix []byte
		prefix, msg.Body = raw.Message[0:2], raw.Message[3:]

		switch string(prefix) {
		case "SS":
			msg.Type = StatusChange
		case "ST":
			msg.Type = TransferStart
		case "CO":
			msg.Type = TransferCompletion
		default:
			logrus.Warn("Unexpected message prefix: ", string(prefix))
			continue
		}
		out <- msg
	}
}

// Inbound returns a channel that can be used to consume monitoring messages already classified
func Inbound(path string) (channel <-chan Message, terminate chan<- bool, err error) {
	dirqHandle, err := dirq.New(path)
	if err != nil {
		return nil, nil, err
	}

	msgchannel := make(chan Message, 100)
	termchannel := make(chan bool)

	go func(channel chan<- Message, terminate <-chan bool) {
		defer close(channel)
		for {
			select {
			case _, _ = <-terminate:
				logrus.Debug("Inbound exiting")
				return
			case <-time.After(1 * time.Second):
				processIncoming(dirqHandle, msgchannel)
			}

		}
	}(msgchannel, termchannel)

	return msgchannel, termchannel, nil
}
