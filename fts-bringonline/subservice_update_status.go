/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/go-dirq"
	"path"
	"strings"
	"time"
)

var messagingDirectory = flag.String("MessagingDirectory", "/var/lib/fts3", "Directory where the internal FTS3 messages are written")
var siteName = flag.String("SiteName", "", "Site name")

// StagingStatusUpdate wraps state changes for a given file
type StagingStatusUpdate struct {
	FileID       uint64
	JobID        string
	State        string
	StagingStart time.Time
	StagingEnd   time.Time
	Reason       error
	Host         string
}

// StagingBulkStatusUpdate groups a set of individual StagingStatusUpdate
type StagingBulkStatusUpdate struct {
	Updates []StagingStatusUpdate
	Ack     chan bool
}

// StagingBulkTokenUpdate groups a bulk update of the request token
type StagingTokenUpdate struct {
	FileIDs      []uint64
	RequestToken string
}

type StagingResponse struct {
	StatusUpdate chan<- *StagingBulkStatusUpdate
	TokenUpdate  chan<- *StagingTokenUpdate
}

// StatusUpdater loops over the messages coming from the status channel, and updates the
// database and send status change messages
func (b *BringOnlineService) StatusUpdater() {
	monitoringPath := path.Join(*messagingDirectory, "monitoring")
	dirq, err := dirq.New(monitoringPath)
	if err != nil {
		log.WithError(err).Fatal("Could not instantiate the dirq directory")
	}

	log.Info("StatusUpdater started")

	for {
		select {
		case statusUpdate := <-b.statusChannel:
			UpdateDbStatus(b.db, statusUpdate)
			SendMessageStatusTransition(b.db, dirq, statusUpdate)
			if statusUpdate.Ack != nil {
				statusUpdate.Ack <- true
				close(statusUpdate.Ack)
			}
		case tokenUpdate := <-b.tokenChannel:
			UpdateDbToken(b.db, tokenUpdate)
		}
	}
}

// SendMessageStatusTransition writes a message to disk with the state for the given file
func SendMessageStatusTransition(db *sql.DB, dirq *dirq.Dirq, bulk *StagingBulkStatusUpdate) {
	for _, update := range bulk.Updates {
		sendMessageForFile(db, dirq, update.FileID)
	}
}

func sendMessageForFile(db *sql.DB, dirq *dirq.Dirq, fileID uint64) {
	l := log.WithField("file_id", fileID)

	type StatusMessage struct {
		Timestamp int64  `json:"timestamp"`
		Endpoint  string `json:"endpnt"`
		FileID    uint64 `json:"file_id"`
		Staging   bool   `json:"staging"`

		JobID        string         `json:"job_id"`
		FileState    string         `json:"file_state"`
		Reason       sql.NullString `json:"reason"`
		StagingStart int64          `json:"staging_start"`
		StagingEnd   int64          `json:"staging_finished"`
		UserDN       string         `json:"user_dn"`
		SourceURL    string         `json:"src_url"`
		DestURL      string         `json:"dst_url"`
		VOName       string         `json:"vo_name"`
		SourceSE     string         `json:"source_se"`
		DestSE       string         `json:"dest_se"`
		UserFilesize sql.NullInt64  `json:"user_filesize"`
		SubmitTime   int64          `json:"submit_time"`

		JobMetadata  json.RawMessage `json:"job_metadata"`
		FileMetadata json.RawMessage `json:"file_metadata"`
	}

	message := StatusMessage{}
	message.Timestamp = time.Now().UnixNano() / 1000
	message.Endpoint = *siteName
	message.FileID = fileID
	message.Staging = true

	rows, err := db.Query(
		`SELECT f.job_id, f.file_state, f.staging_start, f.staging_finished, f.reason,
			 j.user_dn, f.source_surl, f.dest_surl, f.vo_name, f.source_se, f.dest_se,
			 f.user_filesize, j.submit_time, j.job_metadata, f.file_metadata
		FROM t_file f JOIN t_job j ON f.job_id = j.job_id
		WHERE file_id = ?`,
		fileID,
	)
	if err != nil {
		l.WithError(err).Error("Could not query the file")
		return
	}
	defer rows.Close()
	if !rows.Next() {
		l.Error("No data retrieved")
		return
	}

	rows.Scan(
		&message.JobID, &message.FileState, &message.StagingStart, &message.StagingEnd, &message.Reason,
		&message.UserDN, &message.SourceURL, &message.DestURL, &message.VOName, &message.SourceSE, &message.DestSE,
		&message.UserFilesize, &message.SubmitTime, &message.JobMetadata, &message.FileMetadata,
	)

	raw, err := json.Marshal(message)
	if err != nil {
		log.WithError(err).Error("Could not serialize the state message")
		return
	}

	// fts_msg_bulk expects the prefix "SS " so it knows it is a status message
	raw = append([]byte("SS "), raw...)
	err = dirq.Produce(raw)
	if err != nil {
		log.WithError(err).Error("Could not write to the dirq message queue")
	}
	l.Debug("State transition message written to disk")
}

// allowedPreviousStates returns possible previous states for a given state.
// Intended for cross-check state transitions
func allowedPreviousStates(newState string) []string {
	switch newState {
	case Started:
		return []string{Started, Staging}

	case Canceled:
		fallthrough
	case Failed:
		return []string{Staging, Started}

	case Submitted:
		fallthrough
	case Finished:
		return []string{Started}
	}
	return []string{}
}

// UpdateDb updates the database with the new status
func UpdateDbStatus(db *sql.DB, statusUpdate *StagingBulkStatusUpdate) {
	// Avoid updating many times the same job
	uniqueJobIds := make(map[string]bool)

	for _, update := range statusUpdate.Updates {
		log.Infof("STAGING Update : %d %s %s", update.FileID, update.State, update.JobID)
		uniqueJobIds[update.JobID] = true

		allowedPreviousStates := allowedPreviousStates(update.State)
		if len(allowedPreviousStates) == 0 {
			log.Panic("Unexpected state transition: ", update.State)
		}

		fields := []string{"file_state = ?"}
		args := []interface{}{string(update.State)}

		if !update.StagingStart.IsZero() {
			fields = append(fields, "staging_start = ?")
			args = append(args, update.StagingStart)
		}
		if !update.StagingEnd.IsZero() {
			fields = append(fields, "staging_finished = ?")
			args = append(args, update.StagingEnd)
		}
		if update.Reason != nil {
			fields = append(fields, "reason = ?")
			args = append(args, "STAGING "+update.Reason.Error())
		}
		if update.Host != "" {
			fields = append(fields, "staging_host = ?")
			args = append(args, update.Host)
		}

		if IsTerminal(update.State) {
			fields = append(fields, "finish_time = ?")
			args = append(args, time.Now())
		}

		args = append(args, update.FileID)
		for _, allowed := range allowedPreviousStates {
			args = append(args, string(allowed))
		}

		result, err := db.Exec(
			`UPDATE t_file SET `+strings.Join(fields, ", ")+
				` WHERE file_id = ? AND file_state IN (?`+strings.Repeat(", ?", len(allowedPreviousStates)-1)+`)`,
			args...,
		)

		if err != nil {
			log.Error("Failed to update transfer: ", err)
			return
		}

		affected, err := result.RowsAffected()
		if err != nil {
			log.Error("Failed to get the number of affected rows: ", err)
			return
		} else if affected == 0 {
			log.Warn("No transfers updated")
		}
	}

	for jobId := range uniqueJobIds {
		if err := updateJob(db, jobId); err != nil {
			log.WithError(err).Error("Failed to update the job state for ", jobId)
		}
	}
}

func updateJob(db *sql.DB, jobID string) error {
	l := log.WithField("job_id", jobID)

	rows, err := db.Query("SELECT job_state FROM t_job WHERE job_id = ?", jobID)
	if err != nil {
		return err
	}
	rows.Next()
	var storedState string
	if err = rows.Scan(&storedState); err != nil {
		log.Panic(err)
	}
	rows.Close()

	// This code is only worth for the staging part!
	rows, err = db.Query(
		"SELECT file_state, COUNT(*) FROM t_file WHERE job_id = ? GROUP BY file_state ORDER BY NULL", jobID,
	)
	if err != nil {
		return err
	}
	defer rows.Close()

	var nFailed, nFinished, nStaging, nActive, nSubmitted, nCancel, nTotal int
	for rows.Next() {
		var state string
		var count int

		if err = rows.Scan(&state, &count); err != nil {
			l.Panic(err)
		}

		nTotal += count

		switch state {
		case Started:
			fallthrough
		case Staging:
			nStaging = count
		case Submitted:
			nSubmitted = count
		case Ready:
			fallthrough
		case Active:
			nActive += count
		case Failed:
			nFailed = count
		case Canceled:
			nCancel = count
		case Finished:
			nFinished = count
		default:
			l.Panic("Unexpected file state: ", state)
		}
	}

	var newJobState string
	nTerminal := nFinished + nFailed + nCancel

	if nTerminal == nTotal {
		// All terminal, so job state must be terminal
		if nFinished == nTotal {
			newJobState = Finished
		} else if nCancel == nTotal {
			newJobState = Canceled
		} else if nFailed == nTotal {
			newJobState = Failed
		} else {
			newJobState = FinishedDirty
		}
	} else {
		// There are non terminal, which discards non terminal job states
		if nStaging == 0 && nActive == 0 && nSubmitted > 0 {
			newJobState = Submitted
		} else if nStaging == 0 && nActive > 0 {
			newJobState = Active
		} else {
			newJobState = Staging
		}
	}

	// Noop
	if newJobState == storedState {
		l.Debug("No changes to the job state")
		return nil
	}

	// Apply the update. There is a difference between terminal and non-terminal job state!
	var result sql.Result
	if nTerminal == nTotal {
		result, err = db.Exec(
			"UPDATE t_job SET job_state = ?, job_finished =? WHERE job_id = ?",
			newJobState, time.Now(), jobID,
		)
	} else {
		result, err = db.Exec(
			"UPDATE t_job SET job_state = ? WHERE job_id = ?",
			newJobState, jobID,
		)
	}

	affected, err := result.RowsAffected()
	if affected == 0 {
		l.Warn("No records updated")
	}

	l.Info("Job state updated to ", newJobState)

	return err
}

func UpdateDbToken(db *sql.DB, tokenUpdate *StagingTokenUpdate) {
	for _, fileID := range tokenUpdate.FileIDs {
		l := log.WithFields(log.Fields{"file_id": fileID, "request_token": tokenUpdate.RequestToken})
		_, err := db.Exec(
			"UPDATE t_file SET bringonline_token = ? WHERE file_id = ?", tokenUpdate.RequestToken, fileID,
		)
		if err != nil {
			l.WithError(err).Error("Failed to update the request token")
		} else {
			l.Debug("Updated request token")
		}
	}
}
