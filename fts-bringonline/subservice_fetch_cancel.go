/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"time"
	"errors"
)

var ErrCanceled = errors.New("Canceled by the user")

func getForCanceling(db *sql.DB, hashStart, hashEnd int) (tasks []*StagingBatch, err error) {
	var rows *sql.Rows
	rows, err = db.Query(
		`SELECT f.job_id, f.file_id, f.source_surl, f.bringonline_token, j.cred_id
		 FROM t_file f JOIN t_job j ON f.job_id = j.job_id
		 WHERE file_state='CANCELED' AND finish_time IS NULL AND staging_start IS NOT NULL AND
		 	hashed_id BETWEEN ? AND ?`,
		hashStart, hashEnd,
	)
	if err != nil {
		return
	}
	defer rows.Close()

	groupedTasks := make(map[GroupKey]*StagingBatch)
	for rows.Next() {
		file := StagingFile{}
		key := GroupKey{}

		err = rows.Scan(&file.JobID, &file.FileID, &file.SourceURL, &key.RequestToken, &key.CredID)
		if err != nil {
			log.WithError(err).Panic("Failed to scan the canceled files")
		}

		task, found := groupedTasks[key]
		if !found {
			task = &StagingBatch{
				CredID:       key.CredID,
				RequestToken: key.RequestToken.String,
			}
		}
		task.Files = append(task.Files, file)
	}

	for _, task := range groupedTasks {
		tasks = append(tasks, task)
	}

	return
}

// FetchForCanceling looks for staging requests that are to be cancelled
func (b *BringOnlineService) FetchForCanceling() {
	log.Info("FetchForCanceling started")
	for {
		time.Sleep(time.Duration(*stagingSleep) * time.Second)

		forCanceling, err := getForCanceling(b.db, b.hashStart, b.hashEnd)
		if err != nil {
			log.WithError(err).Error("Failed to get stage requests to be canceled")
			continue
		}

		for _, task := range forCanceling {
			go DoCancel(
				context.Background(), &b.credStore, task, ErrCanceled,
				&StagingResponse{StatusUpdate: b.statusChannel},
			)
		}
	}
}
