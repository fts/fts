/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"time"
)

type GroupKey struct {
	StagingQueue
	RequestToken sql.NullString
}

// RecoverAlreadyStaging gets from the database those staging files that have already been
// started
func (b *BringOnlineService) RecoverAlreadyStaging() {
	log.Info("Recovering already started")

	rows, err := b.db.Query(
		`SELECT j.cred_id, f.source_se, f.source_surl, f.dest_surl, f.job_id, f.file_id, f.bringonline_token,
			j.bring_online, f.staging_start
		 FROM t_file f JOIN t_job j ON f.job_id = j.job_id
		 WHERE
		 	f.file_state = 'STARTED' AND
		 	f.hashed_id BETWEEN ? AND ?
		 `,
		b.hashStart, b.hashEnd,
	)
	if err != nil {
		log.WithError(err).Error("Failed to recover stage requests from the database")
		return
	}
	defer rows.Close()

	groupFilesPerRequest := make(map[GroupKey]*StagingBatch)
	var startTime *time.Time

	for rows.Next() {
		groupKey := GroupKey{}

		file := StagingFile{}
		err := rows.Scan(
			&groupKey.CredID, &groupKey.SourceStorage,
			&file.SourceURL, &file.DestURL, &file.JobID, &file.FileID, &groupKey.RequestToken,
			&file.Timeout, &startTime,
		)

		if err != nil {
			log.WithError(err).Panic("Failed to recover already staging files")
		}
		task := groupFilesPerRequest[groupKey]
		if task == nil {
			task = &StagingBatch{
				CredID:       groupKey.CredID,
				RequestToken: groupKey.RequestToken.String,
			}
		}

		task.Files = append(task.Files, file)

		groupFilesPerRequest[groupKey] = task
	}

	if startTime == nil {
		startTime = &time.Time{}
		*startTime = time.Now()
	}

	for _, task := range groupFilesPerRequest {
		ctx, _ := context.WithDeadline(
			context.Background(),
			startTime.Add(time.Duration(task.Files[0].Timeout)*time.Second),
		)
		go DoPoll(ctx, &b.credStore, task, &StagingResponse{StatusUpdate: b.statusChannel})
	}
}
