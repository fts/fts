/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	"flag"
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/fts/fts/daemon"
	"gitlab.cern.ch/fts/fts/log"
	"sync"
)

var runAsUser = flag.String("User", "fts3", "Run as this user")
var runAsGroup = flag.String("Group", "fts3", "Run as this group")
var logDir = flag.String("ServerLogDirectory", "/var/log/fts3", "Log directory")
var logLevel = flag.String("LogLevel", "INFO", "Debug level")
var useStdout = flag.Bool("stdout", false, "Output logging into stdout")

// BringOnlineService models the bring online service and hold internal shared data structures
type BringOnlineService struct {
	db            *sql.DB
	statusChannel chan *StagingBulkStatusUpdate
	tokenChannel  chan *StagingTokenUpdate
	wg            sync.WaitGroup
	firstBeat     chan bool
	credStore     CredentialStoreImpl
	// Updated by the heartbeat, used to know what to run
	nodeCount, nodeIndex int
	hashStart, hashEnd   int
}

// Start all subservices
func (b *BringOnlineService) Start() {
	// Hearbeat needs to be the first one, and everyone should wait for it to do the first beat
	firstBeat := make(chan bool)
	b.wg.Add(1)
	go func() {
		b.Heartbeat(firstBeat)
		b.wg.Done()
	}()

	<-firstBeat
	logrus.Info("Server first beat!")

	b.RecoverAlreadyStaging()

	b.wg.Add(3)
	go func() {
		b.FetchForStaging()
		b.wg.Done()
	}()
	go func() {
		b.StatusUpdater()
		b.wg.Done()
	}()
	go func() {
		b.FetchForCanceling()
		b.wg.Done()
	}()
}

// Wait for all subservices to exit
func (b *BringOnlineService) Wait() {
	b.wg.Wait()
}

func connectDatabase() *sql.DB {
	dbAddress := GenerateDatabaseAddress()
	db, err := sql.Open("mysql", dbAddress)
	if err != nil {
		logrus.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		logrus.Fatal(err)
	}
	logrus.Info("Connected to the database")
	return db
}

// Entry point
func main() {
	// Make sure there is only one process running per machine
	self := daemon.GetBinaryName()
	if daemon.CountProcessesWithName(self) > 1 {
		logrus.Fatalf("Another %s process is already running", self)
	}

	// Load the configuration
	LoadConfiguration()

	// Drop privileges ASAP
	if err := daemon.DropPrivileges(*runAsUser, *runAsGroup); err != nil {
		logrus.Fatal(err)
	}

	// Initialize logging
	log.InitLogging(*logLevel)
	if !*useStdout {
		log.RedirectLog(*logDir, "fts3bringonline.log")
	}

	// Start the service
	db := connectDatabase()
	bringOnline := BringOnlineService{
		db:            db,
		statusChannel: make(chan *StagingBulkStatusUpdate, 100),
		tokenChannel:  make(chan *StagingTokenUpdate, 100),
		credStore:     CredentialStoreImpl{db: db},
	}
	defer bringOnline.db.Close()
	defer close(bringOnline.statusChannel)
	defer close(bringOnline.tokenChannel)

	bringOnline.Start()
	bringOnline.Wait()
	logrus.Fatal("All services exited unexpectedly")
}
