/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	"flag"
	log "github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
	"os"
	"time"
)

var stagingBulkSize = flag.Int("StagingBulkSize", 400, "Maximum bulk size")
var stagingWaitingFactor = flag.Int("StagingWaitingFactor", 5, "Seconds to wait before submitting a bulk request")
var stagingMaxConcurrentRequests = flag.Int("StagingConcurrentRequests", 500, "Maximum number of concurrent requests")
var stagingSleep = flag.Int("StagingSleep", 5, "Time to sleep between iterations")

// getQueuesWithPending returns a list with all queues which have pending files for staging
func getQueuesWithPending(db *sql.DB) (queues []StagingQueueWithCount, err error) {
	rows, err := db.Query(
		`SELECT j.cred_id, f.source_se, f.vo_name, COUNT(*) FROM t_file f JOIN t_job j ON j.job_id = f.job_id
		 WHERE f.file_state = 'STAGING' GROUP BY j.cred_id, f.source_se, f.vo_name ORDER BY NULL`,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		queue := StagingQueueWithCount{}
		err = rows.Scan(&queue.CredID, &queue.SourceStorage, &queue.VOName, &queue.QueuedCount)
		if err != nil {
			log.Panic(err)
		}
		queues = append(queues, queue)
	}
	return
}

// getMaxStagingFiles return how many files, tops, can be done for the given queue
func getMaxStagingFilesForQueue(db *sql.DB, q StagingQueue) int {
	rows, err := db.Query(
		"SELECT concurrent_ops FROM t_stage_req WHERE vo_name = ? AND host = ? AND operation = 'staging'",
		q.VOName, q.SourceStorage,
	)
	defer rows.Close()

	if err == nil && rows.Next() {
		max := 0
		if err = rows.Scan(&max); err == nil {
			return max
		}
	}

	if err != nil {
		log.Warn("getMaxConfigured: ", err)
	}

	return *stagingBulkSize * *stagingMaxConcurrentRequests
}

// countAlreadyStarted returns both how many files, and how many requests, there are active for the given queue
func countAlreadyStarted(db *sql.DB, q StagingQueue) (int, int, error) {
	rows, err := db.Query(
		`SELECT COUNT(*), COUNT(DISTINCT bringonline_token)
		 FROM t_file
		 WHERE vo_name = ? AND source_se = ? AND file_state = 'STARTED'`,
		q.VOName, q.SourceStorage,
	)
	defer rows.Close()
	if err != nil {
		return 0, 0, err
	}
	if !rows.Next() {
		return 0, 0, nil
	}
	startedCount, requestCount := 0, 0
	if err := rows.Scan(&startedCount, &requestCount); err != nil {
		log.Panic(err)
	}
	return startedCount, requestCount, nil
}

// getFilesForStaging return no more than 'bulkSize' number of files waiting to be stages
func getFilesForStaging(db *sql.DB, q StagingQueue, hashStart, hashEnd, bulkSize int) (batch *StagingBatch, err error) {
	batch = &StagingBatch{CredID: q.CredID}

	rows, err := db.Query(`
		SELECT f.source_surl, f.dest_surl, f.job_id, f.file_id, j.copy_pin_lifetime, j.bring_online, j.source_space_token
		FROM v_staging f JOIN t_job j ON f.job_id = j.job_id
		WHERE f.source_se = ? AND j.cred_id = ? AND j.vo_name = ? AND
		      f.hashed_id BETWEEN ? AND ?
		LIMIT ?`,
		q.SourceStorage, q.CredID, q.VOName, hashStart, hashEnd, bulkSize,
	)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		file := StagingFile{}
		err = rows.Scan(
			&file.SourceURL, &file.DestURL, &file.JobID, &file.FileID, &file.PinLifetime,
			&file.Timeout, &file.SpaceToken,
		)
		if err != nil {
			log.Panic(err)
		}
		batch.Files = append(batch.Files, file)
	}

	return
}

// FetchForStaging polls the DB for staging requests, and issue staging operations
func (b *BringOnlineService) FetchForStaging() {
	log.Info("FetchForStaging started")

	// Stores the time when a bulk must be submitted even when there aren't enough to fully fill one
	triggerTimeForQueue := make(map[StagingQueue]time.Time)

	for {
		time.Sleep(time.Duration(*stagingSleep) * time.Second)

		queuedWithPending, err := getQueuesWithPending(b.db)
		if err != nil {
			log.Error("Failed to get queues with pending staging operations: ", err)
			continue
		}

		if len(queuedWithPending) == 0 {
			log.Debug("Nothing to do")
			continue
		}

		for _, q := range queuedWithPending {
			fileLimit := getMaxStagingFilesForQueue(b.db, q.StagingQueue)

			l := log.WithFields(log.Fields{
				"source_se":      q.SourceStorage,
				"cred_id":        q.CredID,
				"vo_name":        q.VOName,
				"queued":         q.QueuedCount,
				"limit_files":    fileLimit,
				"limit_requests": *stagingMaxConcurrentRequests,
			})

			startedCount, requestCount, err := countAlreadyStarted(b.db, q.StagingQueue)
			if err != nil {
				l.WithError(err).Error("Could not get the number of started staging operations")
				continue
			}

			l = l.WithFields(log.Fields{
				"count_started":  startedCount,
				"count_requests": requestCount,
			})

			if startedCount >= fileLimit {
				l.Debug("Reached limit for ", q)
				continue
			}

			// If we haven't got enough for a bulk request, give some extra time for more
			// requests to arrive
			if q.QueuedCount < *stagingBulkSize {
				triggerTime, found := triggerTimeForQueue[q.StagingQueue]
				if !found {
					triggerTime = time.Now().Add(time.Duration(*stagingWaitingFactor) * time.Second)
					triggerTimeForQueue[q.StagingQueue] = triggerTime
				}
				if triggerTime.Sub(time.Now()) > 0 {
					l.WithField("trigger_time", triggerTime).Debug("Not enough queued staging operations, so wait")
					continue
				}
				delete(triggerTimeForQueue, q.StagingQueue)
				l.Info("Waiting time passed")
			}

			// Get a bulk, update file states, and start the staging
			batch, err := getFilesForStaging(b.db, q.StagingQueue, b.hashStart, b.hashEnd, *stagingBulkSize)
			if err != nil {
				l.Error(err)
				continue
			}
			if len(batch.Files) == 0 {
				l.Debug("No staging assigned to this node")
				continue
			}

			l.WithField(
				"bulk_size", len(batch.Files),
			).Info("Issuing bring online for ", q.SourceStorage)

			// We need to update the file state here so the next iteration does not pick them again
			hostname, _ := os.Hostname()
			bulkUpdate := StagingBulkStatusUpdate{
				Updates: make([]StagingStatusUpdate, len(batch.Files)),
				Ack:     make(chan bool),
			}
			for i, file := range batch.Files {
				bulkUpdate.Updates[i] = StagingStatusUpdate{
					FileID:       file.FileID,
					JobID:        file.JobID,
					State:        Started,
					StagingStart: time.Now(),
					Host:         hostname,
				}
				log.Info("BRINGONLINE Issue staging request for ", file.SourceURL)
			}

			b.statusChannel <- &bulkUpdate

			// Block here for the ACK, so we avoid re-scheduling
			_ = <-bulkUpdate.Ack
			log.Debug("ACK received")

			// Separate goroutine for the staging
			ctx, _ := context.WithTimeout(
				context.Background(), time.Duration(batch.Files[0].Timeout)*time.Second,
			)
			go DoStaging(
				ctx, &b.credStore, batch,
				&StagingResponse{StatusUpdate: b.statusChannel, TokenUpdate: b.tokenChannel},
			)
		}
	}
}
