/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

// File and job states
const (
	Started   = "STARTED"
	Staging   = "STAGING"
	Finished  = "FINISHED"
	Failed    = "FAILED"
	Canceled  = "CANCELED"
	Submitted = "SUBMITTED"
	Ready     = "READY"
	Active    = "ACTIVE"

	FinishedDirty = "FINISHEDDIRTY"
)

// StagingFile models a file that is to be staged
type StagingFile struct {
	FileID      uint64
	JobID       string
	SourceURL   string
	DestURL     string
	PinLifetime int
	Timeout     int
	SpaceToken  string
}

// StagingTask groups several StagingFiles together into a single bulk request
type StagingBatch struct {
	CredID       string
	Files        []StagingFile
	RequestToken string
}

// StagingQueue identifies uniquely a staging queue
type StagingQueue struct {
	SourceStorage string
	CredID        string
	VOName        string
}

// StagingQueueWithCount extends StagingQueue with a count on the number of queued files
type StagingQueueWithCount struct {
	StagingQueue
	QueuedCount int
}

// String returns a string representation for the staging queue
func (s StagingQueue) String() string {
	return s.SourceStorage + " " + s.VOName
}

// IsTerminal returns true if the state is a terminal state
func IsTerminal(state string) bool {
	return state == Finished || state == Failed || state == Canceled || state == FinishedDirty
}

// IsBringOnlineOnly returns true if the staging file does not require a transfer phase
func (f *StagingFile) IsBringOnlineOnly() bool {
	return f.DestURL == f.SourceURL
}
