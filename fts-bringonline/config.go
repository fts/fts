package main

import (
	"flag"
	log "github.com/Sirupsen/logrus"
	"github.com/go-sql-driver/mysql"
	"gopkg.in/ini.v1"
	"net"
	"net/url"
)

var fts3config = flag.String("configfile", "/etc/fts3/fts3config", "FTS3 server config file")
var dbUserName = flag.String("DbUserName", "", "Database account user name")
var dbPassword = flag.String("DbPassword", "", "Database account password")
var dbConnectString = flag.String("DbConnectString", "", "Connect string for the used database account")

// LoadConfiguration initializes the configuration using the flags and the configuration file
func LoadConfiguration() {
	flag.Parse()

	ini, err := ini.Load(*fts3config)
	if err != nil {
		log.WithError(err).Fatal("Could not open the configuration file")
	}

	rootSection, err := ini.GetSection("")
	if err != nil {
		log.WithError(err).Fatal("Could not get the root section")
	}

	for _, key := range rootSection.Keys() {
		flag.Set(key.Name(), key.String())
	}

	// Call again to override values loaded from the file
	flag.Parse()
}

// GenerateDatabaseAddress generates a valid Data Source Name from the database parameters
func GenerateDatabaseAddress() string {
	config := mysql.Config{
		User:   *dbUserName,
		Passwd: *dbPassword,
	}
	parsedConnectString, err := url.Parse("mysql://" + *dbConnectString)
	if err != nil {
		log.Fatal("Could not parse the database connect string: ", err)
	}

	host, port, err := net.SplitHostPort(parsedConnectString.Host)
	if err != nil {
		host, port = parsedConnectString.Host, "3306"
	}

	config.Net = "tcp"
	config.Addr = net.JoinHostPort(host, port)
	if len(parsedConnectString.Path) > 0 {
		config.DBName = parsedConnectString.Path[1:]
	}
	config.Params = map[string]string{"parseTime": "true"}
	return config.FormatDSN()
}
