/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"database/sql"
	"flag"
	log "github.com/Sirupsen/logrus"
	"math"
	"os"
	"time"
)

var heartBeatInterval = flag.Int("HeartBeatInterval", 60, "In seconds, interval between heartbeats")
var heartBeatGraceInterval = flag.Int("HeartBeatGraceInterval", 120, "In seconds, after this interval a host is considered down")

// Mark the node as alive and well
func updateHeartBeat(db *sql.DB, service string) (int, int) {
	thishost, err := os.Hostname()
	if err != nil {
		log.WithError(err).Panic("Could not obtain the host name")
	}

	_, err = db.Exec(
		`INSERT INTO t_hosts (hostname, beat, service_name) VALUES (?, UTC_TIMESTAMP(), ?)
		 ON DUPLICATE KEY UPDATE beat = UTC_TIMESTAMP()
		`,
		thishost, service,
	)
	if err != nil {
		log.WithError(err).Panic("Could not update the t_hosts table")
	}

	rows, err := db.Query(
		`SELECT hostname FROM t_hosts
		 WHERE beat >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL ? SECOND) AND service_name = ?
		 ORDER BY hostname`,
		*heartBeatGraceInterval, service,
	)
	if err != nil {
		log.WithError(err).Panic("Could not get the running hosts")
	}
	defer rows.Close()

	nCount := 0
	nIndex := 0
	i := 0
	for rows.Next() {
		var host string
		nCount++
		if err := rows.Scan(&host); err != nil {
			log.WithError(err).Panic("Failed to scan the hostname")
		}
		if host == thishost {
			nIndex = i
		}
		i++
	}

	return nIndex, nCount
}

// return the start and end of the working range for this node
func calculateHashRange(nodeIndex, nodeCount int) (int, int) {
	segsize := math.MaxUint16 / nodeCount
	segmod := math.MaxUint16 % nodeCount

	start := segsize * nodeIndex
	end := segsize*(nodeIndex+1) - 1

	// Last one take over what is left
	if nodeIndex == nodeCount-1 {
		end += segmod + 1
	}

	return start, end
}

// Hearbeat gives signal that this process is alive, and gets the working space for the daemon
func (b *BringOnlineService) Heartbeat(firstBeat chan<-bool) {
	log.WithField("hearbeat_interval", *heartBeatInterval).Info("Hearbeat started")

	b.nodeIndex, b.nodeCount = updateHeartBeat(b.db, "fts_bringonline")
	b.hashStart, b.hashEnd = calculateHashRange(b.nodeIndex, b.nodeCount)
	firstBeat <- true
	close(firstBeat)

	for {
		log.Debugf("Systole: host %d out of %d [%x:%x]", b.nodeIndex, b.nodeCount, b.hashStart, b.hashEnd)
		time.Sleep(time.Duration(*heartBeatInterval) * time.Second)

		b.nodeIndex, b.nodeCount = updateHeartBeat(b.db, "fts_bringonline")
		b.hashStart, b.hashEnd = calculateHashRange(b.nodeIndex, b.nodeCount)
	}
}
