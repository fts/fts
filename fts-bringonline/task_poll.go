/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"errors"
	"flag"
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/dmc/go-gfal2"
	"golang.org/x/net/context"
	"syscall"
	"time"
)

var stagingPollRetries = flag.Int("StagingPollRetries", 3, "Retry this number of times if a staging poll fails with ECOMM")

var ErrTimeout = errors.New("Timeout expired")

// DoPoll polls a remote storage for the status of a bring online request
func DoPoll(ctx context.Context, credStore CredentialStore, batch *StagingBatch, resp *StagingResponse) {
	proxyPath, expirationTime, err := credStore.GetX509ProxyFile(batch.CredID)
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}

	gfal2Context, err := gfal2.NewContext()
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}
	defer gfal2Context.Close()

	gfal2Context.SetOptString("X509", "CERT", proxyPath)
	gfal2Context.SetOptString("X509", "KEY", proxyPath)

	wait := 2 * time.Second
	retriesLeft := *stagingPollRetries
	renewProxyDeadline := time.After(expirationTime.Sub(time.Now()) - 2*time.Minute)

	for {
		log.Infof("BRINGONLINE next attempt in %.0f seconds", wait.Seconds())

		select {
		// Request context canceled or expired
		case <-ctx.Done():
			for _, file := range batch.Files {
				log.Info("Timeout expired for ", file.SourceURL)
			}
			go DoCancel(ctx, credStore, batch, ErrTimeout, resp)
			return

		// Proxy may expire soon, so ask for a newer one if there is
		case <-renewProxyDeadline:
			log.Debug("Proxy may be about to expire, renew on disk")
			proxyPath, expirationTime, err = credStore.GetX509ProxyFile(batch.CredID)
			if err != nil {
				markAllAsFailed(ctx, batch, err, resp)
				return
			}
			gfal2Context.SetOptString("X509", "CERT", proxyPath)
			gfal2Context.SetOptString("X509", "KEY", proxyPath)
			renewProxyDeadline = time.After(expirationTime.Sub(time.Now()) - 2*time.Minute)

		// Do a new poll
		case <-time.After(wait):
			remaining, failed, updates := pollRemote(gfal2Context, batch, &retriesLeft)

			// Update state for those that are terminal
			if resp != nil && resp.StatusUpdate != nil {
				resp.StatusUpdate <- &StagingBulkStatusUpdate{
					Updates: updates,
				}
			}

			// For those that failed, issue a cancel
			if len(failed) > 0 {
				go DoCancel(
					ctx,
					credStore, &StagingBatch{
						CredID:       batch.CredID,
						Files:        failed,
						RequestToken: batch.RequestToken,
					},
					nil, nil,
				)
			}

			// All finished
			if len(remaining) == 0 {
				return
			}
			// Go again, with exponential back-off
			if wait < 30*time.Minute {
				wait *= 2
			}
			batch.Files = remaining

		}
	}
}

// pollRemote polls the remote storage, and returns the remaining files (still queued), failed, and updates
func pollRemote(gfal2Context *gfal2.Context, task *StagingBatch, retriesLeft *int) (remaining []StagingFile, failed []StagingFile, updates []StagingStatusUpdate) {
	urls := make([]string, len(task.Files))
	for i := range task.Files {
		urls[i] = task.Files[i].SourceURL
	}

	errors := gfal2Context.BringOnlinePollList(urls, task.RequestToken)

	remaining = make([]StagingFile, 0, len(task.Files))
	failed = make([]StagingFile, 0, len(task.Files))
	updates = make([]StagingStatusUpdate, 0, len(task.Files))

	gotCommError := false
	for i := range errors {
		if errors[i] == nil || errors[i].Code() == syscall.ENOSYS {
			var nextState string
			if task.Files[i].IsBringOnlineOnly() {
				nextState = Finished
			} else {
				nextState = Submitted
			}
			// Finished
			updates = append(updates, StagingStatusUpdate{
				FileID:     task.Files[i].FileID,
				JobID:      task.Files[i].JobID,
				State:      nextState,
				StagingEnd: time.Now(),
			})
		} else if errors[i].Code() == syscall.EAGAIN {
			log.Info("BRINGONLINE NOT FINISHED for ", task.Files[i].SourceURL)
			remaining = append(remaining, task.Files[i])
		} else if errors[i].Code() == syscall.ECOMM && *retriesLeft > 0 {
			log.Info("BRINGONLINE RETRYING AFTER ECOMM for ", task.Files[i].SourceURL)
			remaining = append(remaining, task.Files[i])
			gotCommError = true
		} else {
			// Failed
			updates = append(updates, StagingStatusUpdate{
				FileID:     task.Files[i].FileID,
				JobID:      task.Files[i].JobID,
				State:      Failed,
				StagingEnd: time.Now(),
				Reason:     errors[i],
			})
			failed = append(failed, task.Files[i])
		}
	}

	if gotCommError {
		*retriesLeft--
	}
	return
}
