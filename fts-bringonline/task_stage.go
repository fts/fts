/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/dmc/go-gfal2"
	"golang.org/x/net/context"
	"syscall"
	"time"
)

// Mark all files in the batch as failed
func markAllAsFailed(ctx context.Context, batch *StagingBatch, reason error, resp *StagingResponse) {
	if resp == nil || resp.StatusUpdate == nil {
		return
	}
	bulkUpdate := StagingBulkStatusUpdate{
		Updates: make([]StagingStatusUpdate, len(batch.Files)),
	}
	for i, file := range batch.Files {
		log.WithError(reason).WithFields(log.Fields{
			"job_id":  file.JobID,
			"file_id": file.FileID,
		}).Error(reason.Error())

		bulkUpdate.Updates[i] = StagingStatusUpdate{
			FileID:       file.FileID,
			JobID:        file.JobID,
			State:        Failed,
			StagingStart: time.Now(),
			StagingEnd:   time.Now(),
			Reason:       reason,
		}
	}
	resp.StatusUpdate <- &bulkUpdate
}

// DoStaging issues a bring online request to a remote storage element, and starts polling
// afterwards if the files are not online
func DoStaging(ctx context.Context, credStore CredentialStore, batch *StagingBatch, resp *StagingResponse) {
	var err error

	log.Infof(
		"BRINGONLINE issuing bring-online for: %d files, with copy-pin-lifetime: %d and bring-online-timeout: %d",
		len(batch.Files), batch.Files[0].PinLifetime, batch.Files[0].Timeout,
	)

	proxyPath, _, err := credStore.GetX509ProxyFile(batch.CredID)
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}

	gfal2Context, err := gfal2.NewContext()
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}
	defer gfal2Context.Close()

	gfal2Context.SetOptString("X509", "CERT", proxyPath)
	gfal2Context.SetOptString("X509", "KEY", proxyPath)

	urls := make([]string, len(batch.Files))
	for i := range batch.Files {
		urls[i] = batch.Files[i].SourceURL
	}

	var errors []gfal2.GError
	batch.RequestToken, errors = gfal2Context.BringOnlineList(
		urls,
		batch.Files[0].PinLifetime,
		batch.Files[0].Timeout,
		true,
	)
	remainingFiles := make([]StagingFile, 0, len(batch.Files))
	statusUpdate := StagingBulkStatusUpdate{
		Updates: make([]StagingStatusUpdate, 0, len(batch.Files)),
	}
	tokenUpdate := StagingTokenUpdate{
		RequestToken: batch.RequestToken,
		FileIDs:      make([]uint64, len(batch.Files)),
	}

	for i := range errors {
		if errors[i] == nil || errors[i].Code() == syscall.ENOSYS {
			var nextState string
			if batch.Files[i].IsBringOnlineOnly() {
				nextState = Finished
			} else {
				nextState = Submitted
			}
			// Finished
			statusUpdate.Updates = append(statusUpdate.Updates, StagingStatusUpdate{
				FileID:       batch.Files[i].FileID,
				JobID:        batch.Files[i].JobID,
				State:        nextState,
				StagingStart: time.Now(),
				StagingEnd:   time.Now(),
			})
		} else if errors[i].Code() == syscall.EAGAIN {
			remainingFiles = append(remainingFiles, batch.Files[i])
		} else {
			// Failed
			statusUpdate.Updates = append(statusUpdate.Updates, StagingStatusUpdate{
				FileID:     batch.Files[i].FileID,
				JobID:      batch.Files[i].JobID,
				State:      Failed,
				StagingEnd: time.Now(),
				Reason:     errors[i],
			})
		}

		tokenUpdate.FileIDs[i] = batch.Files[i].FileID
	}

	if resp != nil {
		if resp.StatusUpdate != nil {
			resp.StatusUpdate <- &statusUpdate
		}
		if resp.TokenUpdate != nil {
			resp.TokenUpdate <- &tokenUpdate
		}
	}

	batch.Files = remainingFiles
	if len(batch.Files) > 0 {
		DoPoll(ctx, credStore, batch, resp)
	}
}
