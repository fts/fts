package main

import (
	"database/sql"
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/go-proxy"
	"io/ioutil"
	"path"
	"time"
)

var (
	ErrProxyExpired  = errors.New("Proxy expired")
	ErrProxyNotFound = errors.New("Proxy not found on the database")
)

// CredentialStore is used to retrieve a path to a file that contains the X509 credential
// identified by credID
type CredentialStore interface {
	GetX509ProxyFile(credID string) (string, time.Time, error)
}

// CredentialStoreImpl implements CredentialStore backed by a SQL database
type CredentialStoreImpl struct {
	db *sql.DB
}

// Return true if path is a proxy that is not expired
func isValidProxy(path string, expirationTime *time.Time) bool {
	proxy := proxy.X509Proxy{}
	if err := proxy.DecodeFromFile(path); err != nil {
		return false
	}
	*expirationTime = proxy.NotAfter
	return !proxy.Expired()
}

// GetProxy returns the proxy associated with credId
func (c *CredentialStoreImpl) GetX509ProxyFile(credId string) (fullpath string, expirationTime time.Time, err error) {
	filename := fmt.Sprint("x509up_h", credId, ".pem")
	fullpath = path.Join("/tmp", filename)

	l := log.WithField("proxy", fullpath)

	if isValidProxy(fullpath, &expirationTime) {
		l.Debug("Proxy still valid")
		return
	}

	var rows *sql.Rows
	rows, err = c.db.Query("SELECT proxy, termination_time FROM t_credential WHERE dlg_id = ?", credId)
	if err != nil {
		return
	}
	defer rows.Close()

	if !rows.Next() {
		l.Debug("Proxy not found on the database")
		err = ErrProxyNotFound
		return
	}

	var pem []byte
	if err := rows.Scan(&pem, &expirationTime); err != nil {
		log.WithError(err).Panic("Failed to scan the proxy")
	}

	if time.Now().After(expirationTime) {
		l.Debug("Proxy expired on the database")
		err = ErrProxyExpired
		return
	}

	l.Debug("Recover proxy from database")
	err = ioutil.WriteFile(fullpath, pem, 0600)
	return
}
