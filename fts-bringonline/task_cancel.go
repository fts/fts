/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.cern.ch/dmc/go-gfal2"
	"golang.org/x/net/context"
	"time"
)

// DoCancel issue a staging cancel request
func DoCancel(ctx context.Context, credStore CredentialStore, batch *StagingBatch, reason error, resp *StagingResponse) {
	proxyPath, _, err := credStore.GetX509ProxyFile(batch.CredID)
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}

	gfal2Context, err := gfal2.NewContext()
	if err != nil {
		markAllAsFailed(ctx, batch, err, resp)
		return
	}
	defer gfal2Context.Close()

	gfal2Context.SetOptString("X509", "CERT", proxyPath)
	gfal2Context.SetOptString("X509", "KEY", proxyPath)

	urls := make([]string, len(batch.Files))
	for i := range batch.Files {
		urls[i] = batch.Files[i].SourceURL
		log.Info("Aborting bringonline for ", urls[i])
	}

	errors := gfal2Context.AbortFiles(urls, batch.RequestToken)

	bulkUpdate := StagingBulkStatusUpdate{
		Updates: make([]StagingStatusUpdate, len(batch.Files)),
	}
	for i := range errors {
		if errors[i] != nil {
			log.WithError(errors[i]).Error("Failed to abort staging request for ", urls[i])
		}
		bulkUpdate.Updates[i] = StagingStatusUpdate{
			FileID:     batch.Files[i].FileID,
			JobID:      batch.Files[i].JobID,
			State:      Canceled,
			StagingEnd: time.Now(),
			Reason:     reason,
		}
	}

	if resp != nil && resp.StatusUpdate != nil {
		resp.StatusUpdate <- &bulkUpdate
	}
}
